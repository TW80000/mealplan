const { Pool } = require('pg');

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
    var r = (Math.random() * 16) | 0,
      v = c == 'x' ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

class RecipeService {
  constructor() {
    this.db = new Pool();
  }

  /**
   * Validates that a given object has the right attributes to be considered a recipe.
   */
  validate(obj) {
    if (!obj.hasOwnProperty('name') || typeof obj.name !== 'string' || obj.name.length < 1)
      throw new Error(`Property 'name' is not valid`);

    if (
      !obj.hasOwnProperty('category') ||
      typeof obj.category !== 'string' ||
      obj.category.length < 1
    )
      throw new Error(`Property 'category' is not valid`);

    if (!obj.hasOwnProperty('servings')) throw new Error(`Property 'servings' is missing`);

    if (typeof obj.servings !== 'number') throw new Error(`Property 'servings' must be a number`);

    if (!Number.isSafeInteger(obj.servings))
      throw new Error(`Property 'servings' must be a safe integer`);

    if (obj.servings < 1) throw new Error(`Property 'servings' must be greater than zero`);

    if (!obj.hasOwnProperty('directions') || typeof obj.directions !== 'string')
      throw new Error(`Property 'directions' is not valid`);

    if (!obj.hasOwnProperty('ingredients') || !Array.isArray(obj.ingredients))
      throw new Error(`Property 'ingredients' is not valid`);

    obj.ingredients.map((ingredient, i) => {
      if (
        !ingredient.hasOwnProperty('name') ||
        typeof ingredient.name !== 'string' ||
        ingredient.name.length < 1
      )
        throw new Error(`For ingredient ${i + 1} the property 'name' is not valid`);

      if (!ingredient.hasOwnProperty('qty'))
        throw new Error(`For ingredient ${ingredient.name} the property 'qty' is missing`);

      if (typeof ingredient.qty !== 'number')
        throw new Error(`For ingredient ${ingredient.name} the property 'qty' must be a number`);

      if (ingredient.qty <= 0)
        throw new Error(
          `For ingredient ${ingredient.name} the property 'qty' must be greater than zero`
        );

      if (!Number.isFinite(ingredient.qty))
        throw new Error(`For ingredient ${ingredient.name} the property 'qty' must be finite`);

      if (!ingredient.hasOwnProperty('unit') || typeof ingredient.unit !== 'string')
        throw new Error(`For ingredient ${ingredient.name} the property 'unit' is not valid`);

      if (!ingredient.hasOwnProperty('preparation'))
        throw new Error(`For ingredient ${ingredient.name} the property 'preparation' is missing`);

      if (typeof ingredient.preparation !== 'string')
        throw new Error(
          `For ingredient ${ingredient.name} the property 'preparation' must be a string`
        );
    });
  }

  async insertRecipe(recipe, username = null) {
    // Make sure the argument is a valid recipe
    this.validate(recipe);

    // Generate a UUID for the recipe. We assume this will be unique
    const recipeId = uuidv4();

    // Insert the recipe into the recipes table
    await this.db.query(
      'INSERT INTO recipes (owner, name, id, directions, category, servings) VALUES ($1, $2, $3, $4, $5, $6)',
      [username, recipe.name, recipeId, recipe.directions, recipe.category, recipe.servings]
    );

    // Insert each ingredient into the ingredients table
    await Promise.all(
      recipe.ingredients.map(ingredient => {
        return this.db.query(
          'INSERT INTO ingredients (name, qty, recipe_id, unit, preparation) VALUES ($1, $2, $3, $4, $5)',
          [ingredient.name, ingredient.qty, recipeId, ingredient.unit, ingredient.preparation]
        );
      })
    );
  }

  async deleteRecipeById(id) {
    // TODO: Cascade deletion
    await this.db.query('DELETE FROM ingredients WHERE recipe_id = $1', [id]);
    await this.db.query('DELETE FROM recipes WHERE id = $1', [id]);
  }

  async getRecipeById(id) {
    // First, get all recipes for that user
    const recipes = (await this.db.query('SELECT * FROM recipes WHERE id = $1', [id])).rows;

    if (recipes.length === 0) {
      throw new Error(`No recipe with id = ${id} found`);
    }

    // Second, populate the ingredients field for each recipe
    for (var i = 0; i < recipes.length; i++) {
      recipes[i].ingredients = (await this.db.query(
        'SELECT * FROM ingredients WHERE recipe_id = $1',
        [recipes[i].id]
      )).rows;
    }

    return recipes[0];
  }

  async getRecipesForUser(username) {
    // First, get all recipes for that user
    const recipes = (await this.db.query('SELECT * FROM recipes WHERE owner = $1', [username]))
      .rows;

    // Second, populate the ingredients field for each recipe
    for (var i = 0; i < recipes.length; i++) {
      recipes[i].ingredients = (await this.db.query(
        'SELECT * FROM ingredients WHERE recipe_id = $1',
        [recipes[i].id]
      )).rows;
    }

    return recipes;
  }

  async generateGroceryList(recipeIds = []) {
    // FIXME : There's probably a way to do this with a single SQL statement
    let ingList = (await Promise.all(
      recipeIds.map(id => {
        return this.db.query('SELECT * FROM ingredients WHERE recipe_id = $1', [id]);
      })
    )).map(x => x.rows);

    // Flatten
    ingList = ingList.reduce((p, c) => p.concat(c), []);

    // Convert floats that end in .00 to ints
    ingList = ingList.map(ingredient => {
      // Convert floats with .00 to ints
      if (ingredient.qty.indexOf('.00') !== -1) {
        // FIXME
        ingredient.qty = parseInt(ingredient.qty);
      }

      // Show common fractions as fractions instead of floats
      switch (ingredient.qty) {
        case '0.25':
          ingredient.qty = '¼';
          break;
        case '0.50':
          ingredient.qty = '½';
          break;
        case '1.50':
          ingredient.qty = '1 ½';
          break;
      }

      return ingredient;
    });

    // Sum and convert

    // const ingredients = {};

    // ingList.map(ingredient => {
    //     if (!ingredients.hasOwnProperty(ingredient.name)) {
    //         ingredients[ingredient.name] = ingredient.qty;
    //     } else {
    //         ingredients[ingredient.name] += convert('ml', ingredient.unit, ingredient.qty);
    //     }
    // });

    // const groceryList = Object.keys(ingredients).map(ingredient => {
    //     return {
    //         name: ingredient,
    //         unit: 'ml',
    //         qty: ingredients[ingredient]
    //     };
    // });

    // console.log(groceryList);

    // return groceryList;

    return ingList;
  }

  /** WARNING: Modifies the given object by reference */
  prettifyRecipe(recipe) {
    // NOTE: We don't validate here because we expect to be prettifying a
    // recipe object whose values came from the db, meaning all numerical
    // fields will be strings and validation would fail.

    for (let i = 0; i < recipe.ingredients.length; i++) {
      // Convert floats with .00 to ints
      if (recipe.ingredients[i].qty.indexOf('.00') !== -1) {
        // FIXME
        recipe.ingredients[i].qty = parseInt(recipe.ingredients[i].qty);
      }

      // Show common fractions as fractions instead of floats
      switch (recipe.ingredients[i].qty) {
        case '0.25':
          recipe.ingredients[i].qty = '¼';
          break;
        case '0.50':
          recipe.ingredients[i].qty = '½';
          break;
        case '1.50':
          recipe.ingredients[i].qty = '1 ½';
          break;
      }
    }

    return recipe;
  }
}

const conversionTable = {
  mL: {
    L: 1000,
  },
};

function convert(toUnit, fromUnit, num) {
  return num * conversionTable[fromUnit][toUnit];
}

module.exports = new RecipeService();
