const bcrypt = require('bcryptjs');
const { Pool } = require('pg');

class UserService {
  constructor() {
    this.db = new Pool();
  }

  async getUserByName(username) {
    const res = await this.db.query(`SELECT * FROM users WHERE username = $1`, [username]);
    return (res.rows.length === 1) ? res.rows[0] : undefined;
  }

  async setUserToken(username, token) {
    await this.db.query(`UPDATE users SET session_token = $1 WHERE username = $2`, [token, username]);
  }

  async getUserByToken(token) {
    const res = await this.db.query('SELECT * FROM users WHERE session_token = $1', [token]);
    return (res.rows.length === 1) ? res.rows[0] : undefined;
  }

  async createUser(username, password) {
    const salt = bcrypt.genSaltSync(10);
    const password_hashed = bcrypt.hashSync(password, salt);
    await this.db.query('INSERT INTO users (username, password_hashed) VALUES ($1, $2)', [username, password_hashed]);
  }
}

module.exports = new UserService();