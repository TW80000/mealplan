const express = require('express');
const router = express.Router();

const UserService = require('../services/UserService');

router.get('/', (req, res) => {
    UserService.setUserToken(req.cookies.username, null);
    res.clearCookie('username');
    res.clearCookie('token');
    res.redirect('./?logout=true');
});

module.exports = router;
