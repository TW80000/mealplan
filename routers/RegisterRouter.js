const express = require('express');
const UserService = require('../services/UserService');
const bcrypt = require('bcryptjs');

const router = express.Router();

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = (Math.random() * 16) | 0,
      v = c == 'x' ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

router.get('/', async (req, res) => {
  return res.render('register', {
    base: process.env.base,
    isAuthenticated: req.isAuthenticated,
  });
});

router.post('/', async (req, res) => {
  try {
    if (
      !req.body.hasOwnProperty('username') ||
      !(typeof req.body.username === 'string') ||
      !(req.body.username.length > 0)
    ) {
      // No username
      return res.status(400).json({
        message: 'Username cannot be blank',
        messageType: 'error',
      });
    }

    if (
      !req.body.hasOwnProperty('password') ||
      !(typeof req.body.password === 'string') ||
      !(req.body.password.length > 0)
    ) {
      // No password
      return res.status(400).json({
        message: 'Password cannot be blank',
        messageType: 'error',
      });
    }

    const user = await UserService.getUserByName(req.body.username);

    // Username already taken
    if (user !== undefined) {
      res.status(403).json({
        message: 'That username is already taken',
        messageType: 'error',
      });
    }

    // Create new user
    await UserService.createUser(req.body.username, req.body.password);

    // Happy path, login successful
    const token = uuidv4();
    UserService.setUserToken(req.body.username, token);
    return res
      .status(200)
      .cookie('token', token)
      .cookie('username', req.body.username)
      .redirect('/');
  } catch (err) {
    console.error(err);
    return res.status(500).json({
      message: 'Login failed due to server error: ' + err.message,
      messageType: 'error',
    });
  }
});

module.exports = router;
