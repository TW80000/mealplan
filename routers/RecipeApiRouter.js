const express = require('express');
const router = express.Router();

const RecipeService = require('../services/RecipeService');

// TODO: Put this in a separate file.
function authenticate(req, res, next) {
  if (!req.isAuthenticated) {
    return res.redirect(`/login?redirectTo=${req.originalUrl}`);
  } else {
    next();
  }
}

router.get('/:owner', async (req, res) => {
  try {
    const recipes = await RecipeService.getRecipesForUser(req.params.owner);
    res.status(200).json({ recipes: recipes });
  } catch (err) {
    res.status(500).json({ message: `Server Error: ${err}`, type: 'error' });
  }
});

router.post('/', async (req, res) => {
  try {
    await RecipeService.insertRecipe(req.body, req.cookies.username || null);
  } catch (err) {
    res.status(400).json({ message: `Not a valid recipe: ${err}`, type: 'error' });
  }
  res.status(200).json({ message: 'Thanks!', type: 'success' });
});

router.post('/generategrocerylist', async (req, res) => {
  try {
    const groceryList = await RecipeService.generateGroceryList(req.body.recipeIds);
    res.status(200).json({ groceryList: groceryList });
  } catch (err) {
    res.status(500).json({ message: `Server Error: ${err}`, type: 'error' });
  }
});

router.delete('/:id', authenticate, async (req, res) => {
  const id = req.params.id;
  try {
    await RecipeService.deleteRecipeById(id);
    res.status(200).json({
      message: 'Successfully deleted recipe.',
      type: 'success',
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      message: 'Failed to delete recipe.',
      type: 'success',
    });
  }
});

router.delete('/');

module.exports = router;
