const express = require('express');
const UserService = require('../services/UserService');
const bcrypt = require('bcryptjs');

const router = express.Router();

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = (Math.random() * 16) | 0,
      v = c == 'x' ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

router.get('/', (req, res) => {
  return res.render('login', {
    redirectTo: req.query.redirectTo,
    base: process.env.base,
    isAuthenticated: req.isAuthenticated,
  });
});

router.post('/', async (req, res) => {
  try {
    if (
      !req.body.hasOwnProperty('username') ||
      !(typeof req.body.username === 'string') ||
      !(req.body.username.length > 0)
    ) {
      // No username
      return res.render('login', {
        base: process.env.base,
        isAuthenticated: req.isAuthenticated,
        banner: {
          message: 'Must provide username',
          type: 'error',
        },
      });
    }

    if (
      !req.body.hasOwnProperty('password') ||
      !(typeof req.body.password === 'string') ||
      !(req.body.password.length > 0)
    ) {
      // No password
      return res.render('login', {
        base: process.env.base,
        isAuthenticated: req.isAuthenticated,
        banner: {
          message: 'Must provide password',
          type: 'error',
        },
      });
    }

    const user = await UserService.getUserByName(req.body.username);

    // User doesn't exist
    if (user === undefined) {
      return res.render('login', {
        base: process.env.base,
        isAuthenticated: req.isAuthenticated,
        banner: {
          message: 'User not found',
          type: 'error',
        },
      });
    }

    // Incorrect password
    if (!(await bcrypt.compare(req.body.password, user.password_hashed))) {
      return res.render('login', {
        base: process.env.base,
        isAuthenticated: req.isAuthenticated,
        banner: {
          message: 'Incorrect password',
          type: 'error',
        },
      });
    }

    // Happy path, login successful
    const token = uuidv4();
    UserService.setUserToken(user.username, token);
    const redirectTo = req.body.redirectTo || '/';
    return res
      .status(200)
      .cookie('token', token)
      .cookie('username', user.username)
      .redirect(`.${redirectTo}`);
  } catch (err) {
    console.error(err);
    return res.status(500).json({
      message: 'Login failed due to server error: ' + err.message,
      messageType: 'error',
    });
  }
});

module.exports = router;
