Vue.component('add-recipe-card', {
  props: ['recipe'],
  template: `
    <div class="add-recipe-card card">
      <span>{{ recipe.name }}</span>
      <button class="btn success" v-on:click="$emit('add-recipe', recipe.id)">Make this</button>
    </div>
  `,
});

function servingCardDragStart(evt) {
  evt.dataTransfer.setData('text/plain', evt.target.id);
  evt.dataTransfer.dropEffect = 'move';
}

Vue.component('serving-card', {
  props: ['id', 'setId', 'recipe'],
  template: `
    <div v-bind:id="id" class="card" draggable="true" ondragstart="servingCardDragStart(event);">
      <span>{{ recipe.name }}</span>
      <button class="btn error" v-on:click="$emit('delete-set', setId)">
        <i class="fas fa-trash"></i>
      </button>
    </div>
  `,
});

var APP = new Vue({
  el: '#app',
  data: {
    userRecipes: RECIPES,
    others: [{ id: 'Eat out', name: 'Eat out', servings: 1 }],
    servings: [], // These are the recipes selected by the user to work with.
    idCounter: 1, // Used to generate ids for working recipes.
    search: '',
    mondayServings: [],
    tuesdayServings: [],
    wednesdayServings: [],
    thursdayServings: [],
    fridayServings: [],
    saturdayServings: [],
    sundayServings: [],
  },
  methods: {
    addServingsForRecipe: function(id) {
      const recipe = this.recipes.find(recipe => recipe.id === id);
      const setId = `set${this.idCounter++}`;
      for (var i = 0; i < recipe.servings; i++) {
        console.log(setId);
        this.servings = this.servings.concat([
          {
            id: `sc${this.idCounter++}`,
            setId: setId,
            recipe: recipe,
          },
        ]);
      }
    },
    deleteSet: function(id) {
      [
        'servings',
        'mondayServings',
        'tuesdayServings',
        'wednesdayServings',
        'thursdayServings',
        'fridayServings',
        'saturdayServings',
        'sundayServings',
      ].forEach(prop => {
        APP[prop] = APP[prop].filter(serving => serving.setId !== id);
      });
    },
  },
  computed: {
    recipes: function() {
      if (this.search === '') return this.userRecipes.concat(this.others);
      return this.userRecipes
        .filter(recipe => {
          const search = this.search.toLowerCase();
          return (
            recipe.name.toLowerCase().includes(search) ||
            recipe.category.toLowerCase().includes(search)
          );
        })
        .concat(this.others);
    },
    breakfasts: function() {
      return this.recipes.filter(r => r.category === 'Breakfast');
    },
    lunches: function() {
      return this.recipes.filter(r => r.category === 'Lunch');
    },
    dinners: function() {
      return this.recipes.filter(r => r.category === 'Dinner');
    },
    desserts: function() {
      return this.recipes.filter(r => r.category === 'Dessert');
    },
    snacks: function() {
      return this.recipes.filter(r => r.category === 'Snacks');
    },
    drinks: function() {
      return this.recipes.filter(r => r.category === 'Drinks');
    },
  },
});

// TODO: Generate grocery list from servings.
