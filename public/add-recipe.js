const addIngredientButton = document.getElementById('addIngredientButton');
const recipeNameElm = document.getElementById('recipe-name');
const recipeCategoryElm = document.getElementById('recipe-category');
const recipeServingsElm = document.getElementById('recipe-servings');
const recipeDirectionsElm = document.getElementById('recipe-directions');
const previewElm = document.getElementById('preview-elm');
const previewBtn = document.getElementById('preview-button');
const submitBtn = document.getElementById('submit-button');
const ingredientsDiv = document.getElementById('ingredients');

submitBtn.addEventListener('click', submitRecipeToServer);
previewBtn.addEventListener('click', displayRecipe);
addIngredientButton.addEventListener('click', addIngredient);

function addIngredient() {
  ingredientsDiv.appendChild(createIngredientElement());
}

function deleteIngredient(evt) {
  evt = evt || window.event;
  const target = evt.target || evt.srcElement;
  target.parentElement.remove();
}

/**
 * Reads the values in the HTML form and returns a valid recipe object
 */
function getRecipeObj() {
  const ingredientElms = Array.from(document.getElementsByClassName('ingredient'));
  const recipe = {};
  recipe.name = recipeNameElm.value;
  recipe.category = recipeCategoryElm.value;
  recipe.servings = recipeServingsElm.value;
  if (!/^[1-9]([0-9]+)?$/.test(recipe.servings)) {
    alert(`Servings must be a positive, non-zero integer`);
    return null;
  } else {
    recipe.servings = Number(recipe.servings);
  }
  recipe.directions = recipeDirectionsElm.value;
  try {
    recipe.ingredients = ingredientElms
      .filter(elm => !elm.id) // Filter out the template
      .map(elm => {
        const qtyValue = elm.children[0].value;
        const unit = elm.children[1].value;
        const name = elm.children[2].value;
        const preparation = elm.children[3].value;
        if (!/^[0-9]+(\.[0-9]([0-9])?)?$/.test(qtyValue)) {
          throw new Error(
            `Ingredient quantity for ${name} must be a positive non-zero decimal or integer with a maximum of two decimal places`
          );
        }
        const qty = Number(qtyValue);
        if (qty <= 0 || !Number.isFinite(qty)) {
          throw new Error(`Ingredient quantity for ${name} must be positive, finite, and non-zero`);
        }
        return {
          qty: qty,
          unit: unit,
          name: name,
          preparation: preparation,
        };
      });
  } catch (err) {
    alert(err);
    return null;
  }
  return recipe;
}

function displayRecipe() {
  const recipe = getRecipeObj();
  if (recipe === null) return;
  // TODO: Support preparation.
  const lis = recipe.ingredients
    .map(ingredient => `  <li>${ingredient.qty} ${ingredient.unit}\t${ingredient.name}</li>`)
    .join('\n');
  let html = `<h1>${recipe.name}</h1><p>Makes ${recipe.servings} serving${
    recipe.servings === 1 ? '' : 's'
  }</p><ul>${lis}</ul><pre>${recipe.directions}</pre>`;
  previewElm.innerHTML = html;
}

function createIngredientElement() {
  const template = document.getElementById('ingredient-template');
  const copy = template.cloneNode(true);
  copy.removeAttribute('id');
  return copy;
}

async function submitRecipeToServer() {
  try {
    const recipe = getRecipeObj();
    if (recipe === null) return;
    const response = await fetch('./api/recipes', {
      method: 'POST',
      headers: new Headers({ 'Content-Type': 'application/json' }),
      body: JSON.stringify(recipe),
      credentials: 'include',
    });
    try {
      const responseJson = await response.json();
      alert(responseJson.message);
    } catch (err) {
      alert("Couldn't parse response into JSON");
      alert(err);
    }
  } catch (err) {
    alert('Error: Request failed');
  }
}
