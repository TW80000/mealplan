// Search

const APP = new Vue({
  el: '#app',
  data: {
    recipes: RECIPES, // The list of all recipes.
    search: '', // The search text.
  },
  computed: {
    filteredRecipes: function() {
      if (this.search === '') return this.recipes;
      return this.recipes.filter(recipe => {
        const search = this.search.toLowerCase();
        return (
          recipe.name.toLowerCase().includes(search) ||
          recipe.category.toLowerCase().includes(search)
        );
      });
    },
  },
});

const SEARCH_ELM = document.getElementById('search');
SEARCH_ELM.focus();

// Delete buttons

Array.from(document.querySelectorAll('button.delete')).map(elm =>
  elm.addEventListener('click', async evt => {
    const id = evt.target.getAttribute('data-recipe-id');
    const result = await deleteRecipe(id);
    if (result) {
      APP.recipes = APP.recipes.filter(recipe => recipe.id !== id);
    }
  })
);

async function deleteRecipe(id) {
  try {
    const response = await fetch(`./api/recipes/${id}`, {
      method: 'DELETE',
      credentials: 'include',
    });
    try {
      const responseJson = await response.json();
      alert(responseJson.message);
      return responseJson.type === 'success';
    } catch (err) {
      alert("Couldn't parse response into JSON");
      console.error(err);
    }
  } catch (err) {
    console.error(err);
    alert('Error: Request failed');
  }
  return false;
}
