# Mealplan

Select which meals you want to make, then Mealplan generates a shopping list for you. You check off what you already have then head to the grocery store. Mealplan will tell you how meals you'll get and approximately what day you'll make it to with the food you selected.

## Running the server

### Installing the Web Server Dependencies

NPM will install all of the required dependencies.

```shell
npm i
```

### Setting the Environment Variables

Add the following to `~/.bash_profile`:

```shell
BASE=https://example.com/
PGUSER=postgres
PGHOST=localhost
PGPASSWORD=<YOUR_PASSWORD>
PGDATABASE=mealplan
PGPORT=5432
```

All of these are configurable obviously.

### Running the web server

```shell
node mealplan-server.js
```
