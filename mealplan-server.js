/**
 * A simple express server to test the mealplan router and service.
 */

const express = require('express');
const bodyParser = require('body-parser');
const server = express();
const path = require('path');
const cookieParser = require('cookie-parser');
const dotenv = require('dotenv').config();

const userService = require('./services/UserService.js');
const recipeService = require('./services/RecipeService.js');

server.set('view engine', 'pug');

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));
server.use(cookieParser());

server.use(express.static(path.posix.join(__dirname, 'public')));

server.use('*', async (req, res, next) => {
  req.isAuthenticated = await userService.getUserByToken(req.cookies.token);
  next();
});

function authenticate(req, res, next) {
  if (!req.isAuthenticated) {
    return res.redirect(`/login?redirectTo=${req.originalUrl}`);
  } else {
    next();
  }
}

server.get('/', (req, res) => {
  var rtn = {
    base: process.env.BASE,
    isAuthenticated: req.isAuthenticated,
  };
  if (req.query.logout === 'true') {
    rtn.banner = {
      message: 'Successfully logged out',
      type: 'success',
    };
  }
  res.render('index', rtn);
});

server.get('/your-recipes', authenticate, async (req, res) => {
  var recipes = await recipeService.getRecipesForUser(req.cookies.username);
  return res.render('your-recipes', {
    recipes: recipes,
    base: process.env.BASE,
    isAuthenticated: req.isAuthenticated,
  });
});

server.get('/add-recipe', authenticate, async (req, res) => {
  return res.render('add-recipe', {
    base: process.env.BASE,
    isAuthenticated: req.isAuthenticated,
  });
});

server.get('/plan', authenticate, async (req, res) => {
  const recipes = await recipeService.getRecipesForUser(req.cookies.username);
  return res.render('plan', {
    recipes: recipes,
    base: process.env.BASE,
    isAuthenticated: req.isAuthenticated,
    __USERNAME__: req.cookies.username, // FIXME: Part of a suboptimal solution
  });
});

server.use('/login', require('./routers/LoginRouter'));

server.use('/register', require('./routers/RegisterRouter'));

server.use('/logout', authenticate, require('./routers/LogoutRouter'));

server.use('/api/recipes', authenticate, require('./routers/RecipeApiRouter'));

server.get('/recipe/:id', authenticate, async (req, res) => {
  const id = req.params.id;
  const recipe = recipeService.prettifyRecipe(await recipeService.getRecipeById(id));
  res.render('recipe', {
    recipe: recipe,
    base: process.env.BASE,
    isAuthenticated: req.isAuthenticated,
  });
});

server.listen(8080, () => console.log('Listening on port 8080.'));
